<?php 
	require 'database-config.php';
	session_start();
	$department = "";
	$username = "";
	$password = "";
	
	if(isset($_POST['username'])){
		$username = $_POST['username'];
	}
	if (isset($_POST['password'])) {
		$password = $_POST['password'];
	}
	//echo $username ." : ".$password;
	$q = 'SELECT * FROM users WHERE username=:username AND password=:password AND status=1';
	$query = $dbh->prepare($q);
	$query->execute(array(':username' => $username, ':password' => $password ));
	if($query->rowCount() == 0){
		header('Location: index.php?err=1');
	}else{
		$row = $query->fetch(PDO::FETCH_ASSOC);
		session_regenerate_id();
		$_SESSION['sess_user_id'] = $row['id'];
		$_SESSION['sess_username'] = $row['username'];
		$_SESSION['sess_signatures'] = $row['picture'];
        $_SESSION['sess_userrole'] = $row['role'];
        //echo $_SESSION['sess_userrole'];
		session_write_close();
		if( $_SESSION['sess_userrole'] == "admin"){
			
			header('Location: admin/index.php');
		}if( $_SESSION['sess_userrole'] == "staff"){
			
			header('Location: staff/index.php');
		}
		if( $_SESSION['sess_userrole'] == "user"){
			
			header('Location: user/index.php');
		}
	}
?>